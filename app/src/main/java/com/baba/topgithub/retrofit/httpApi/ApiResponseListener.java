package com.baba.topgithub.retrofit.httpApi;

import com.baba.topgithub.ui.main.TrendingDevDataModel;

import java.util.Map;

public interface ApiResponseListener {


    public void onApiResponse(TrendingDevDataModel trendingDevDataModel);

    public void onApiError(ApiException apiException);

    public void onFormValidationError(Map<String, String> formValidation);

    public void onServerError(ServerException serverException);
}

package com.baba.topgithub.retrofit;


import com.baba.topgithub.ui.main.TrendingDevDataModel;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {

    @GET("developers")
    public Single<List<TrendingDevDataModel>> getTrendingDevDataApi(@Query("language") String language, @Query("since") String since);


}

package com.baba.topgithub

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.baba.topgithub.databinding.ActivityMainBinding
import com.baba.topgithub.ui.main.TrendingDevViewModel
import com.baba.topgithub.ui.main.developerDetails.interfacess.TrendingDevelopersNavigator
import com.baba.topgithub.ui.main.trendingDevelopers.adapter.TrendingDevAdapter
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject
import javax.inject.Provider

class MainActivity : BaseActivity<ActivityMainBinding, TrendingDevViewModel>(), TrendingDevelopersNavigator {

    @Inject
    lateinit var trendingDevViewModel: TrendingDevViewModel

    @Inject
    internal lateinit var trendingDevAdapter: TrendingDevAdapter


    @Inject
    lateinit var mLayoutManager: Provider<RecyclerView.LayoutManager>

    override fun getBindingVariable(): Int {
        return BR.viewModel
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_main
    }

    override fun getViewModel(): TrendingDevViewModel? {
        return trendingDevViewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        trendingDevViewModel.navigator = this
        recyclerViewSetUp()
        subscribeToLiveData()

        trendingDevViewModel.getTrendingDevData("java", "weekly")
    }

    private fun recyclerViewSetUp() {
        trendingDevRecycler.apply {
            layoutManager = mLayoutManager.get()
            adapter = trendingDevAdapter
            addItemDecoration(DividerItemDecoration(this@MainActivity,
                    DividerItemDecoration.VERTICAL))
        }
    }

    private fun subscribeToLiveData() {
        trendingDevViewModel.devRepos.observe(this,
                Observer { trendingDevViewModel.addTrendingDevToList(it!!) })

    }

    override fun developersListSuccess() {
        //  TODO("not implemented")
    }

}

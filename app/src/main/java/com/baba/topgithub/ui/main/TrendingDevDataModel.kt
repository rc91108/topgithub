package com.baba.topgithub.ui.main

import android.os.Parcelable
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.baba.topgithub.BR

import com.baba.topgithub.ui.main.model.RepoDataModel
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TrendingDevDataModel(var username: String?, var name: String?
                                , var type: String?, var url: String?, var avatar: String?, var repo: RepoDataModel?) : Parcelable, BaseObservable() {
    constructor() : this("", "",
            "", "", "",
            null
    )

    @get:Bindable
    var cusername: String? = username
        set(value) {
            field = value
            notifyPropertyChanged(BR.cusername)
        }

    @get:Bindable
    var curl: String? = url
        set(value) {
            field = value
            notifyPropertyChanged(BR.curl)
        }

    @get:Bindable
    var cname: String? = name
        set(value) {
            field = value
            notifyPropertyChanged(BR.cname)
        }

    @get:Bindable
    var ctype: String? = type
        set(value) {
            field = value
            notifyPropertyChanged(BR.ctype)
        }

    @get:Bindable
    var cavatar: String? = avatar
        set(value) {
            field = value
            notifyPropertyChanged(BR.cavatar)
        }

    @get:Bindable
    var crepo: RepoDataModel? = repo
        set(value) {
            field = value
            notifyPropertyChanged(BR.crepo)
        }
}


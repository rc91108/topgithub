package com.baba.topgithub.ui.main.model;

import android.widget.ImageView;

import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableField;

import com.baba.topgithub.ui.main.TrendingDevDataModel;
import com.squareup.picasso.Picasso;

public class TrendingDevItemViewModel {

    public final DevViewModelListener eventViewModelListener;

    public final ObservableField<String> devTitle = new ObservableField<>();

    public final ObservableField<String> devImage = new ObservableField<>();
    public final ObservableField<String> repoName = new ObservableField<>();

    private final TrendingDevDataModel devModel;


    public TrendingDevItemViewModel(TrendingDevDataModel devModel, DevViewModelListener eventViewModelListener) {
        this.devModel = devModel;
        this.devTitle.set(devModel.getName());
        this.devImage.set(devModel.getAvatar());
        this.repoName.set(devModel.getRepo().getName());
        this.eventViewModelListener = eventViewModelListener;
    }

    public void onItemClick() {
        eventViewModelListener.onItemClick(devModel);
    }


    public interface DevViewModelListener {

        void onItemClick(TrendingDevDataModel event);

    }
}

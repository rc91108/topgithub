package com.baba.topgithub.ui.main.developerDetails.module;

import com.baba.topgithub.ui.main.developerDetails.DevDetailViewModel;

import dagger.Module;
import dagger.Provides;

@Module
public class DevDetailActivityModule {
    @Provides
    DevDetailViewModel provideDetailViewModel() {
        return new DevDetailViewModel();
    }


}

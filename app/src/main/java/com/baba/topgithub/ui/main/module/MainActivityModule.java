package com.baba.topgithub.ui.main.module;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.baba.topgithub.MainActivity;
import com.baba.topgithub.ui.main.TrendingDevViewModel;
import com.baba.topgithub.ui.main.trendingDevelopers.adapter.TrendingDevAdapter;

import dagger.Module;
import dagger.Provides;

@Module
public class MainActivityModule {
    @Provides
    TrendingDevViewModel provideDevViewModel() {
        return new TrendingDevViewModel();
    }

    @Provides
    TrendingDevAdapter provideMediaAdapter() {
        return new TrendingDevAdapter();
    }


    @Provides
    RecyclerView.LayoutManager providRecyclerLayoutManager(MainActivity activity) {
        return new LinearLayoutManager(activity);
    }
}

package com.baba.topgithub.ui.main.trendingDevelopers.adapter

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.baba.topgithub.BaseViewHolder
import com.baba.topgithub.databinding.AdapterTrendingDevBinding
import com.baba.topgithub.ui.main.TrendingDevDataModel
import com.baba.topgithub.ui.main.developerDetails.DevDetailActivity
import com.baba.topgithub.ui.main.model.TrendingDevItemViewModel
import com.baba.topgithub.utils.Constant
import kotlinx.android.synthetic.main.adapter_trending_dev.view.*

class TrendingDevAdapter : RecyclerView.Adapter<BaseViewHolder>() {

    internal var mOpenSourceResponseList: List<TrendingDevDataModel> = mutableListOf()

    fun addData(devDataModels: List<TrendingDevDataModel>) {
        mOpenSourceResponseList = devDataModels
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val itemView = AdapterTrendingDevBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) = holder.onBind(position, mOpenSourceResponseList)

    override fun getItemCount(): Int = mOpenSourceResponseList.size


    inner class ViewHolder(private val mBinding: AdapterTrendingDevBinding) : BaseViewHolder(mBinding.root),
            TrendingDevItemViewModel.DevViewModelListener {

        private var devItemViewModel: TrendingDevItemViewModel? = null

        override fun onBind(position: Int, dataObject: Any?) {
            devItemViewModel = TrendingDevItemViewModel(mOpenSourceResponseList[position], this)
            this.mBinding.trendingDevModel = devItemViewModel
            this.mBinding.executePendingBindings()
        }

        override fun onItemClick(model: TrendingDevDataModel) {
            itemView.context
            val mIntent = Intent(itemView.context, DevDetailActivity::class.java)
            val bundle = Bundle()
            bundle.putParcelable(Constant.IntentKey.MEDAI_DETAIL, model)
            mIntent.putExtra(Constant.IntentKey.MY_BUNDLE, bundle)
            itemView.context.startActivity(mIntent)
        }
    }
}

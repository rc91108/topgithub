package com.baba.topgithub.ui.main

import android.util.Log
import androidx.databinding.ObservableArrayList
import androidx.lifecycle.MutableLiveData
import com.baba.topgithub.BaseViewModel
import com.baba.topgithub.retrofit.RetrofitRestClient
import com.baba.topgithub.ui.main.developerDetails.interfacess.TrendingDevelopersNavigator
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers

open class TrendingDevViewModel : BaseViewModel<TrendingDevelopersNavigator>() {

    val devListModel = ObservableArrayList<TrendingDevDataModel>()
    val devRepos: MutableLiveData<List<TrendingDevDataModel>> = MutableLiveData()

    fun addTrendingDevToList(openSourceItems: List<TrendingDevDataModel>) {
        devListModel.clear()
        devListModel.addAll(openSourceItems)
    }

    fun getTrendingDevData(language: String, since: String) {
        setIsLoading(true)
        getEventObservable(language, since).subscribe(
                { apiResponse ->
                    setIsLoading(false)
                    apiResponse.let {
                        devRepos.value = it
                        navigator.developersListSuccess()
                    }
                },
                { error ->
                    Log.d("TrendingDevViewModel", error.message ?: "onUnknownError")
                }
        ).addTo(compositeDisposable)
    }

    open fun getEventObservable(language: String, since: String): Single<List<TrendingDevDataModel>> {
        return RetrofitRestClient.getInstance()
                .getTrendingDevDataApi(language, since)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}

package com.baba.topgithub.ui.main.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
data class RepoDataModel(var name: String, var description: String, var url: String) : Parcelable

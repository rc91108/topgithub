package com.baba.topgithub.ui.main.developerDetails

import android.os.Bundle
import androidx.lifecycle.Observer
import com.baba.topgithub.BR
import com.baba.topgithub.BaseActivity
import com.baba.topgithub.R
import com.baba.topgithub.databinding.ActivityDevDetailBinding
import com.baba.topgithub.ui.main.TrendingDevDataModel
import com.baba.topgithub.utils.Constant
import javax.inject.Inject

class DevDetailActivity : BaseActivity<ActivityDevDetailBinding, DevDetailViewModel>() {

    @Inject
    lateinit var devDetailViewModel: DevDetailViewModel

    override fun getBindingVariable(): Int {
        return BR.detailviewModel
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_dev_detail
    }

    override fun getViewModel(): DevDetailViewModel {
        return devDetailViewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setData()
    }

    private fun setData() {
        val bundle = intent.getBundleExtra(Constant.IntentKey.MY_BUNDLE)
        val trendingDevDataModel = bundle?.getParcelable<TrendingDevDataModel>(Constant.IntentKey.MEDAI_DETAIL) as TrendingDevDataModel
        devDetailViewModel.devModel.apply {
            cusername = trendingDevDataModel.username
            cname = trendingDevDataModel.name
            ctype = trendingDevDataModel.type
            cavatar = trendingDevDataModel.avatar
            curl = trendingDevDataModel.url
            crepo = trendingDevDataModel.repo
        }
    }
}
package com.baba.topgithub;


import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import io.reactivex.annotations.NonNull;

public abstract class BaseViewHolder extends RecyclerView.ViewHolder {

    public BaseViewHolder(@NonNull View itemView) {
        super(itemView);
    }

    public abstract void onBind(int position, Object dataObject);

}

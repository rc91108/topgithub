package com.baba.topgithub.databinding;


import android.text.TextUtils;
import android.widget.ImageView;

import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.baba.topgithub.ui.main.TrendingDevDataModel;
import com.baba.topgithub.ui.main.trendingDevelopers.adapter.TrendingDevAdapter;
import com.squareup.picasso.Picasso;

import java.util.List;

public class BindingUtils {

    @BindingAdapter("adapter")
    public static void addMediaItems(RecyclerView recyclerView, List<TrendingDevDataModel> openSourceItems) {

        TrendingDevAdapter adapter = (TrendingDevAdapter) recyclerView.getAdapter();
        if (adapter != null) {
            adapter.addData(openSourceItems);
        }
    }

    @BindingAdapter("imageUrl")
    public static void loadImage(ImageView view, String imageUrl) {
        if (!TextUtils.isEmpty(imageUrl))
            Picasso.with(view.getContext())
                    .load(imageUrl)
                    /// .placeholder(R.drawable.placeholder)
                    .into(view);
    }

}

package com.baba.topgithub.rxJava;


import com.baba.topgithub.retrofit.httpApi.ApiException;

public interface BaseRxView {
    void showLoading(String message);

    void onApiError(ApiException error);

    void onUnknownError(String error);

    void onTimeout();

    void onRequestComplete();

    void onNetworkError();


}
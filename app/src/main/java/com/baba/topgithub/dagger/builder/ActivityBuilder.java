package com.baba.topgithub.dagger.builder;


import com.baba.topgithub.MainActivity;
import com.baba.topgithub.ui.main.developerDetails.DevDetailActivity;
import com.baba.topgithub.ui.main.developerDetails.module.DevDetailActivityModule;
import com.baba.topgithub.ui.main.module.MainActivityModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = {MainActivityModule.class})
    abstract MainActivity bindMainActivity();

    @ContributesAndroidInjector(modules = {DevDetailActivityModule.class})
    abstract DevDetailActivity bindDevDetailActivity();
}
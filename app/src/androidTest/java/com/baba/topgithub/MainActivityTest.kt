package com.baba.topgithub


import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.rule.ActivityTestRule
import com.baba.topgithub.CustomAssertions.Companion.hasItemCount
import com.baba.topgithub.CustomMatchers.Companion.withItemCount
import org.junit.Rule
import org.junit.Test

class MainActivityTest {
  @Rule
  @JvmField
  var activityRule = ActivityTestRule<MainActivity>(MainActivity::class.java)


  @Test
  fun countPrograms() {
    onView(withId(R.id.trendingDevRecycler))
        .check(matches(withItemCount(0)))
  }

  @Test
  fun countProgramsWithViewAssertion() {
    onView(withId(R.id.trendingDevRecycler))
        .check(hasItemCount(0))
  }
}

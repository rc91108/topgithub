package com.baba.topgithub

import com.baba.topgithub.ui.main.TrendingDevDataModel
import com.baba.topgithub.ui.main.TrendingDevViewModel
import com.baba.topgithub.ui.main.model.RepoDataModel
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import io.reactivex.Single
import org.junit.Test
import org.mockito.Mockito.`when`

class TrendingDevUseCaseTest {

    @Test
    fun `trending dev, useCase execute`() {
        val mock = mock<TrendingDevViewModel>()
        val list = ArrayList<TrendingDevDataModel>()
        list.apply {
            add(TrendingDevDataModel("JakeWharton", "Jake", "", "", "", RepoDataModel("butterKn", "", "")))
            add(TrendingDevDataModel("Chris", "Chris", "", "", "", null))
            add(TrendingDevDataModel("rok", "rok", "", "", "", null))
        }

        `when`(mock.getEventObservable("java", "weekly")).thenReturn(Single.just(list))

        mock.getEventObservable("java", "weekly")
                .test()
                .assertNoErrors()
                .assertComplete()
                .assertValue(list)

        verify(mock).getEventObservable("java", "weekly")
    }
}
package com.baba.topgithub

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.baba.topgithub.retrofit.ApiService
import com.baba.topgithub.ui.main.TrendingDevDataModel
import com.baba.topgithub.ui.main.TrendingDevViewModel
import com.baba.topgithub.ui.main.model.RepoDataModel
import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import io.reactivex.Observable
import io.reactivex.Single
import junit.framework.Assert.assertEquals
import okhttp3.OkHttpClient
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class TrendingDevRetroUseCaseTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    private lateinit var trendingDevViewModel: TrendingDevViewModel
    private var apiService = providesRetrofit()


    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        trendingDevViewModel = TrendingDevViewModel()
    }


    @Test
    fun `live data, trending dev success`() {
        val value = listOf(TrendingDevDataModel("joshd", "Josh", "user", "", "", null))
        trendingDevViewModel.devRepos.postValue(value)
        assertEquals(value, (trendingDevViewModel.devRepos.value))
    }

    @Test
    fun `live data, trending dev news error`() {
      //  val value = Throwable("error")
     //   Mockito.`when`(trendingDevViewModel.getEventObservable("java", "weekly")).thenReturn(Single.error(value))
    }

    @Test
    fun `check, remote data values`() {
        val list: MutableList<TrendingDevDataModel> = mutableListOf()
        list.apply {
            add(TrendingDevDataModel("joshd", "Josh", "user", "", "", null))
            add(TrendingDevDataModel("Chris", "Chris", "", "", "", null))
            add(TrendingDevDataModel("Nikola", "Nikola Irinchev", "", "", "", RepoDataModel("Realm", "", "")))
        }
        val trendingDevViewModel: TrendingDevViewModel = mock()
        given { trendingDevViewModel.getEventObservable("java", "weekly") }.willReturn(Single.just(list))

        trendingDevViewModel.getEventObservable("java", "weekly")
                .test()
                .assertNoErrors()
                .assertComplete()
                .assertValue(list)

        verify(trendingDevViewModel).getEventObservable("java", "weekly")
    }


    @Test
    fun `network request`() {
        val result = apiService.getTrendingDevDataApi("java", "since")
                .map { data ->
                    data.map { "username: ${it.username}, name: ${it.name}, url: ${it.url}, avatar: ${it.avatar}, repo: ${it.repo}\n" }
                }.blockingGet()
        print(result)
    }


    private fun providesRetrofit(): ApiService {
        return Retrofit.Builder()
                .baseUrl("https://github-trending-api.now.sh/")
                .client(OkHttpClient.Builder().build())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(ApiService::class.java)
    }
}